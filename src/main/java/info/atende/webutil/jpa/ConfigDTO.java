package info.atende.webutil.jpa;

/**
 * Config Data Transfer Objects.
 * All configurations that need to be transfered in a structed way, need to implement this interface. <br >
 * This is used to create sub-sets of configurations that will be tranfered, by example as a Json or XML entity,
 * or manipulated in a structured way. ConfigDTO could be converted to Config using a {@link info.atende.webutil.jpa.ConfigUtils} class
 * before it is saved
 * @author Giovanni Silva
 */
public interface ConfigDTO {
    /**
     * The unique config name. The programmer need to ensure this name is unique, or it can't be stored
     * @return A unique config name.
     */
    public String configName();
}
