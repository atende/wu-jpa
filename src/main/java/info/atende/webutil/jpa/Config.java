package info.atende.webutil.jpa;

import javax.enterprise.inject.Vetoed;
import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Entity representing configuration to be stored. This could be used in a JPA 2 compatible ORM.
 * @see info.atende.webutil.jpa.ConfigUtils
 * @see info.atende.webutil.jpa.ConfigDTO
 * @author Giovanni Silva
 */
@Entity
@Cacheable
@Vetoed
public class Config {
    @Id
    private String config;
    @ElementCollection(fetch = FetchType.EAGER)
    @Column(name="value")
    @CollectionTable(name="config_values")
    private Map<String, String> values;

    public Config(String config, Map<String, String> values) {
        this.config = config;
        this.values = values;
    }

    public Config() {
        values = new HashMap<String, String>();
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }

    public Map<String, String> getValues() {
        return values;
    }

    public void setValues(Map<String, String> values) {
        this.values = values;
    }

}
