package info.atende.webutil.jpa;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.ConstructorUtils;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.logging.Logger;

/**
 * Utility to convert {@link info.atende.webutil.jpa.ConfigDTO} em {@link info.atende.webutil.jpa.Config} and vice-versa
 * <pre>
 * {@code
 * EmailConfig emailConfig = new EmailConfig("servidor_valor", "usuario_valor", "senha_valor");
 * Config config = ConfigUtils.convertToConfig(emailConfig);
 * EmailConfig config2 = ConfigUtils.parseConfig(config);
 * }
 * </pre>
 * @author Giovanni Silva
 */
public abstract class ConfigUtils {
    private static Logger logger = Logger.getLogger(ConfigUtils.class.getName());
    private static final String ERROR_CONVERT_CONFIGDTO = "Can't convert ConfigDTO, %s  to Config\nError: %s";
    private static final String ERROR_CONVERT_CONFIG = "Can't convert Config, %s  to ConfigDTO, %s\nError: %s";
    /**
     * This BeanUtils can convert ENUMs
     */
    private static BeanUtilsBean beanUtilsBean = new BeanUtilsBean(new ConvertUtilsBean(){
        @Override
        public Object convert(String value, Class clazz) {
            if (clazz.isEnum()){
                return Enum.valueOf(clazz, value);
            }else{
                return super.convert(value, clazz);
            }
        }
    });

    /**
     * Transform a ConfigDTO in Config. All JavaBean properties in ConfigDTO will be parsed in to config HashMap
     * by their names
     * @param configDTO Object
     * @return A new Config
     */
    public static Optional<Config> convertToConfig(ConfigDTO configDTO) {
        if(configDTO == null || configDTO.configName() == null){
            return Optional.empty();
        }
        Config config = new Config();
        config.setConfig(configDTO.configName());
        Map<String,String> props = null;
        try {
            props = beanUtilsBean.describe(configDTO);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchElementException | NoSuchMethodException e) {
            logger.warning(String.format(ERROR_CONVERT_CONFIGDTO, configDTO, e.getMessage()));
            return Optional.empty();
        }
        config.setValues(props);
        return Optional.of(config);
    }

    /**
     * Transform a Config in ConfigDTO. <br>
     * All config values will be mapped to properties following the JavaBean conventions
     * @param config The Config Object
     * @param klass The ConfigDTO class to be created, it need to have a default constructor
     * @param <T> klass Instance type
     * @return An instance of ConfigDTO klass
     */
    public static <T extends ConfigDTO> Optional<T> parseConfig(Config config, Class<T> klass) {
        try {
            T instance = ConstructorUtils.invokeConstructor(klass, null);
            beanUtilsBean.populate(instance, config.getValues());
            return Optional.of(instance);
        } catch (Exception e) {
            logger.warning(String.format(ERROR_CONVERT_CONFIG, config, klass, e.getMessage()));
            return Optional.empty();
        }
    }

    /**
     * Load and convert a Config from Database
     * @param em EntityManager to connect with database. The entitymanager should not be null
     * @param klass The ConfigDTO class for conversion
     * @param configName The config name for the ConfigDTO, or the primary key to search. This need to be the configName for the ConfigDTO class passed as argument
     * @param <T> ConfigDTO
     * @return The parsed config if it exists in database
     */
    public static <T extends ConfigDTO> Optional<T> loadFromDatabase(@NotNull EntityManager em, @NotNull Class<T> klass, @NotNull String configName){
        Config config = em.find(Config.class, configName);
        return parseConfig(config, klass);
    }

}
