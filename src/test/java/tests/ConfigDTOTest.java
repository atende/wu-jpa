package tests;

import info.atende.webutil.jpa.Config;
import info.atende.webutil.jpa.ConfigUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.Optional;

/**
 * Teste para ConfigUtils
 * Criado por Giovanni Silva <giovanni@atende.info>
 */
public class ConfigDTOTest {
    /**
     * Testa a conversao de ConfigDTO para Config
     */
    @Test
    public void convertToConfig(){
        EmailConfig emailConfig = new EmailConfig("servidor_valor", "usuario_valor", "senha_valor");
        Config config = ConfigUtils.convertToConfig(emailConfig).get();
        Assert.assertEquals("servidor_valor", config.getValues().get("host"));
        Assert.assertEquals("usuario_valor", config.getValues().get("login"));
        Assert.assertEquals("senha_valor", config.getValues().get("password"));
        Assert.assertEquals(config.getConfig(), emailConfig.configName());
        Optional<Config> config1 = ConfigUtils.convertToConfig(null);
        Assert.assertFalse(config1.isPresent());

        emailConfig = new EmailConfig("google.com", Protocol.TLS, 465, "login", "password", true,
                "no-reply@pucminas.br", true);

        Config convert = ConfigUtils.convertToConfig(emailConfig).get();
        Assert.assertEquals(EmailConfig.CONFIG_NAME, convert.getConfig());
        Assert.assertEquals("google.com", convert.getValues().get("host"));
        Assert.assertEquals("TLS", convert.getValues().get("protocol"));
        Assert.assertEquals("465", convert.getValues().get("port"));
        Assert.assertEquals("login", convert.getValues().get("login"));
        Assert.assertEquals("password", convert.getValues().get("password"));
        Assert.assertEquals("true", convert.getValues().get("needAuthentication"));
        Assert.assertEquals("no-reply@pucminas.br", convert.getValues().get("sender"));
        Assert.assertEquals("true", convert.getValues().get("debug"));

    }

    /**
     * Testa a conversao de Config em ConfigDTO
     */
    @Test
    public void convertFromConfig(){
        EmailConfig emailConfig = new EmailConfig("servidor_valor", "usuario_valor", "senha_valor");
        Config config = ConfigUtils.convertToConfig(emailConfig).get();
        EmailConfig emailConfig1 = ConfigUtils.parseConfig(config, EmailConfig.class).get();
        Assert.assertEquals(emailConfig1.getPassword(), "senha_valor");
        Assert.assertEquals(emailConfig1.getHost(), "servidor_valor");
        Assert.assertEquals(emailConfig1.getLogin(), "usuario_valor");
        Assert.assertEquals(emailConfig1.configName(), config.getConfig());

        Config config1 = new Config();
        config1.getValues().put("login","usuario_valor");
        config1.getValues().put("notfound","not_found");
        EmailConfig emailConfig2 = ConfigUtils.parseConfig(config1, EmailConfig.class).get();
        Assert.assertEquals("usuario_valor", emailConfig2.getLogin());

        emailConfig = new EmailConfig("google.com", Protocol.SMTP, 465, "login", "password", true,
                "no-reply@pucminas.br", true);

        config = new Config();
        config.setConfig(EmailConfig.CONFIG_NAME);
        config.getValues().put("host", "google.com");
        config.getValues().put("protocol","SMTP");
        config.getValues().put("login","login");
        config.getValues().put("password","password");
        config.getValues().put("needAuthentication","true");
        config.getValues().put("sender","no-reply@pucminas.br");
        config.getValues().put("debug","true");
        config.getValues().put("port","465");

        EmailConfig converted = ConfigUtils.parseConfig(config, EmailConfig.class).get();

        Assert.assertEquals(emailConfig.getHost(), converted.getHost());
        Assert.assertEquals(emailConfig.getProtocol(), converted.getProtocol());
        Assert.assertEquals(emailConfig.getLogin(), converted.getLogin());
        Assert.assertEquals(emailConfig.getPassword(), converted.getPassword());
        Assert.assertEquals(emailConfig.getSender(), converted.getSender());
        Assert.assertEquals(emailConfig.getDebug(), converted.getDebug());
        Assert.assertEquals(emailConfig.getNeedAuthentication(), converted.getNeedAuthentication());
        Assert.assertEquals(emailConfig.getPort(), converted.getPort());
        Assert.assertEquals(emailConfig, converted);
    }


    @Test
    public void convertBetweenEmailConfigAndConfig(){
        EmailConfig emailConfig = new EmailConfig("google.com", Protocol.TLS, 465, "login", "password", true,
                "no-reply@pucminas.br", true);
        Config config = ConfigUtils.convertToConfig(emailConfig).get();
        EmailConfig converted = ConfigUtils.parseConfig(config, EmailConfig.class).get();
        Assert.assertEquals(converted, emailConfig);

    }


}
