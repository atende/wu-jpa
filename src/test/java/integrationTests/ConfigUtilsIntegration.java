package integrationTests;

import info.atende.webutil.jpa.Config;
import info.atende.webutil.jpa.ConfigUtils;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import tests.EmailConfig;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Integration Tests for ConfigUtils
 * @author Giovanni Silva
 * Date: 8/30/14.
 */
@RunWith(Arquillian.class)
public class ConfigUtilsIntegration {
    @Deployment
    public static Archive<?> createTestArchive(){
        Archive<?> archive = ShrinkWrap.create(WebArchive.class)
                .addPackage(info.atende.webutil.jpa.Config.class.getPackage())
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource("jbossas-ds.xml")
                .addAsWebInfResource("META-INF/beans.xml", "beans.xml");
        return archive;
    }
    @PersistenceContext
    EntityManager em;
    @Inject
    UserTransaction utx;

    @Before
    public void preparePersistenceTest() throws Exception{
        clearData();
        insertData();
        startTransaction();
    }
    @After
    public void commitTransaction() throws Exception {
        utx.commit();
    }

    private void clearData() throws Exception {
        utx.begin();
        em.joinTransaction();
        System.out.println("Dumping old records...");
        em.createQuery("delete from Config").executeUpdate();
        utx.commit();
    }

    private void insertData() throws Exception {
        utx.begin();
        em.joinTransaction();
        System.out.println("Inserting records...");
        Map<String,String> values = new HashMap<>();
        values.put("host", "server_value");
        values.put("port","456");
        Config config = new Config(EmailConfig.CONFIG_NAME, values);
        em.persist(config);
        utx.commit();
        // clear the persistence context (first-level cache)
        em.clear();
    }

    private void startTransaction() throws Exception {
        utx.begin();
        em.joinTransaction();
    }
    @Test
    public void shouldReturnEntityFromCache(){
        ConfigUtils.loadFromDatabase(em, EmailConfig.class, EmailConfig.CONFIG_NAME);
        Assert.assertTrue(em.getEntityManagerFactory().getCache().contains(Config.class, EmailConfig.CONFIG_NAME));
    }
    @Test
    public void shouldReturnEntityFromDatabase(){
        Optional<EmailConfig> emailConfigOptional = ConfigUtils.loadFromDatabase(em, EmailConfig.class, EmailConfig.CONFIG_NAME);
        Assert.assertTrue(emailConfigOptional.isPresent());
        Assert.assertEquals("server_value",emailConfigOptional.get().getHost());
    }
}
